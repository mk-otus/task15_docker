# task15_docker

Задача 1:
- Создайте свой кастомный образ nginx на базе alpine. После запуска nginx должен
отдавать кастомную страницу (достаточно изменить дефолтную страницу nginx)
- Определите разницу между контейнером и образом
- Вывод опишите в домашнем задании.
- Ответьте на вопрос: Можно ли в контейнере собрать ядро?
- Собранный образ необходимо запушить в docker hub и дать ссылку на ваш
репозиторий.

--------------------------------------------------------------------------------
1. Устанавливаем докер `yum install -y docker`
2. Делаем свой образ из Dockerfile-а
```
for Ubuntu
cat Dockerfile

FROM	docker.io/ubuntu:latest
MAINTAINER My custom Nginx container <rm_axe@mail.ru>
RUN	apt-get update -y \
	&& apt-get install -y nginx
RUN	rm -f /var/www/html/index.nginx-debian.html
COPY	./index.html /var/www/html
CMD ["nginx", "-g", "daemon off;"]



```
и поднимаем контейнер

```
docker build -t my_nginx . && docker run -p 80:80 -d --name my_container my_nginx

docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
a95a5303584e        my_nginx            "nginx -g 'daemon ..."   23 minutes ago      Up 23 minutes       0.0.0.0:80->80/tcp   my_container

```
Разница между образом и контейнером  
```
Image - Unioned Read-only File System
Container(stopped) - Unioned RW File System
Conteiner(started) - Unioned RW File System + созданный namespace и cgroups под контейнер 
```
Можно ли в контейнере собрать ядро?  - можно, но загрузиться с него нельзя (используется ядро хостовой машины)


Теперь запушим свой образ в Docker Hub
```
docker login --username=maxkosh --email=rm_axe@mail.ru
Flag --email has been deprecated, will be removed in 1.14.
Password:
Login Succeeded

docker tag 7ea8bd2c63a3 maxkosh/my_nginx:firsttry

docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
maxkosh/my_nginx                 firsttry            7ea8bd2c63a3        23 hours ago        152 MB
my_nginx                         latest              7ea8bd2c63a3        23 hours ago        152 MB

docker push maxkosh/my_nginx
The push refers to a repository [docker.io/maxkosh/my_nginx]
```


