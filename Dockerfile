FROM	docker.io/ubuntu:latest

MAINTAINER My custom Nginx container <rm_axe@mail.ru>

RUN	apt-get update -y \
	&& apt-get install -y nginx
RUN	rm -f /var/www/html/index.nginx-debian.html

COPY	./index.html /var/www/html

CMD ["nginx", "-g", "daemon off;"]